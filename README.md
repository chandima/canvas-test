## Canvas Integration with Redis Caching

### Install Redis
```
// install Hmebrew on macOS (if needed)
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

// install Redis
$ brew install redis

// start redis server
$ redis-server /usr/local/etc/redis.conf

// try out redis CLI commands in another terminal
$ redis-cli
127.0.0.1:6379> set foo bar
OK
127.0.0.1:6379> get foo
"bar"
```

### Run test app
```
// install
$ npm i

// set Canvas Base URL and DEV Key in environment file .env
CANVAS_API_TOKEN=....
CANVAS_BASE_URL=.....

// run it (redis server needs to be running)
$ npm run start

// Make a few calls and check load times (cache and non cached)

// Courses
http://localhost:5000/course/25
http://localhost:5000/course/80

// Accounts
http://localhost:5000/account/1
http://localhost:5000/account/80
http://localhost:5000/account/sis_account_id:ul-au-dev // search by SIS ID
```