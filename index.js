require('dotenv').config()
const express = require('express');
const canvas = require('canvas-api-wrapper');
const Redis = require("ioredis");

const PORT = process.env.PORT || 5000;
const CANVAS_API_TOKEN = process.env.CANVAS_API_TOKEN;
canvas.baseUrl = process.env.CANVAS_BASE_URL || 'https://canvas-dev.asu.edu'

const app = express();
const redis = new Redis();

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`)
})

async function getAccount(req, res, next) {
  try {
    console.log('Fetching data...');
    const {id} = req.params;
    const account = await canvas(`/api/v1/accounts/${id}`);
    // Set data to Redis (TTL 3600)
    const key = req.url;
    redis.set(key, JSON.stringify(account), "EX", 3600);
    // return result
    res.json(account);
  } catch (error) {
    console.log(error);
    res.status(500)
  }
}

async function getCourse(req, res, next) {
  try {
    console.log('Fetching data...');
    const {id} = req.params;
    const course = await canvas(`/api/v1/courses/${id}`);
    // Set data to Redis (TTL 3600)
    const key = req.url;
    redis.set(key, JSON.stringify(course), "EX", 3600);
    // return result
    res.json(course);
  } catch (error) {
    console.log(error);
    res.status(500)
  }
}

// Cache middleware
function cache(req, res, next) {
  const key = req.url;
  redis.get(key, (err, data) => {
    if (err) throw err;
    if (data !== null) {
      res.json(JSON.parse(data));
    } else {
      next();
    }
  })
}


app.get('/', (req, res, next) => {
  res.send(`<h1>Hello World!</h1>`)
});

// get account details
app.get('/account/:id', cache, getAccount)

// get courses under account
app.get('/course/:id', cache, getCourse)